-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 24 2020 г., 22:50
-- Версия сервера: 5.5.62
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Структура таблицы `ab`
--

DROP TABLE IF EXISTS `ab`;
CREATE TABLE `ab` (
  `id` int(11) NOT NULL,
  `id_avtor` int(11) NOT NULL,
  `id_books` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab`
--

INSERT INTO `ab` (`id`, `id_avtor`, `id_books`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 7),
(4, 3, 8),
(5, 3, 6),
(6, 3, 5),
(7, 3, 4),
(8, 3, 9),
(9, 3, 10),
(10, 3, 11),
(11, 3, 12),
(12, 2, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `autors`
--

DROP TABLE IF EXISTS `autors`;
CREATE TABLE `autors` (
  `id` int(11) NOT NULL,
  `FIO` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `autors`
--

INSERT INTO `autors` (`id`, `FIO`) VALUES
(1, 'ergerg'),
(2, 'regerg'),
(3, 'rgerg'),
(4, 'ergerg');

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `Name`, `Date`) VALUES
(1, 'regerg', '2020-01-01'),
(2, 'regreg', '2020-01-02'),
(3, 'regrge', '2020-01-02'),
(4, 'ewgewg', '2020-01-04'),
(5, 'htre', '2020-01-05'),
(6, 'ryth', '2020-01-06'),
(7, 'rge', '2020-01-14'),
(8, 'erg', '2020-01-07'),
(9, 'qwe1', '2020-01-08'),
(10, 'qwe2', '2020-01-03'),
(11, 'qwe3', '2020-01-22'),
(12, 'qwe4', '2020-01-03');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ab`
--
ALTER TABLE `ab`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_avtor` (`id_avtor`),
  ADD KEY `id_books` (`id_books`);

--
-- Индексы таблицы `autors`
--
ALTER TABLE `autors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ab`
--
ALTER TABLE `ab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `autors`
--
ALTER TABLE `autors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ab`
--
ALTER TABLE `ab`
  ADD CONSTRAINT `ab_ibfk_2` FOREIGN KEY (`id_books`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `ab_ibfk_1` FOREIGN KEY (`id_avtor`) REFERENCES `autors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
